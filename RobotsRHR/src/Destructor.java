
public class Destructor extends MarkJava{
	
	
	
	
	
	/*------------Clases abstractes--------------*/

	public void interactua(MarkJava unAltreRobot) {
		if (obteEnergia() > 5 && unAltreRobot instanceof Destructor 
			&& sorteig.nextInt(4) % 2 == 0) {
			gastaEnergia(3);
			unAltreRobot.gastaBatería();
		} else {
			unAltreRobot.gastaBatería();
		}
	}
	
	public boolean decideixSiMou() {
		if(sorteig.nextInt(10) <= 4 && obteEnergia() >= 5) return true;
		return false;
	}
	
	
	
}
