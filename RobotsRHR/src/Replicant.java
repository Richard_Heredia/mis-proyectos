
public class Replicant extends MarkJava{

	MarkJava robotQueReplicara;
	
	/*------------Clases abstractes--------------*/

	public void interactua(MarkJava unAltreRobot) {
		robotQueReplicara=unAltreRobot;
	}
	
	/*------------Clases abstractes--------------*/
	
	public MarkJava construeix() {
		MarkJava nourobot = null;
		if(obteEnergia()>=7 && robotQueReplicara!=null) {
			nourobot = robotQueReplicara;
		}
		else if(robotQueReplicara==null) {
			int robotRandom = sorteig.nextInt(3);
			switch (robotRandom) {
			case 1:
				nourobot = new Mecanic();
				break;
			case 2:
				nourobot= new Destructor();
				break;
			case 3:
				nourobot = new Replicant();
				break;
			}
		}
		
		return nourobot;
		
	}
	
	public boolean decideixSiMou() {
		if(obteEnergia()>=7 && sorteig.nextInt(10)<=10)
		return true;
		
		return false;
	}
	
	
}
