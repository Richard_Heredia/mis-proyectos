import java.util.Random;

public class Taller {

	private MarkJava[][] robotsJava = new MarkJava[5][5];
	Random movimentRandom = new Random();
	
	
	public static void main(String[] args) {
		Taller jocTaller = new Taller();
		int numTorns=5;
		
		System.out.println("Estat inicial!");
		jocTaller.creaTaller();
		for(int i = 0; i < numTorns; i++) {
			System.out.println("Torn "+ (i+1));
			jocTaller.torn();
		}
	}
	
	
	/*------------funcions extres del main--------------*/
	
	public void resolMoviment(MarkJava robotAMoure, int x, int y) {
		//todo moviment es decideix en el metode: decideixSiMou

			//primero vemos si el robot es replicante o no, para saber si solo movernos o
			//en su defecto, hacer el robot replica
			int moviment = movimentRandom.nextInt(3);
			
			if(robotAMoure instanceof Replicant == true ) {
				
				Replicant robotReplicant = (Replicant)(robotsJava[x][y]);
				switch (moviment) {
				case 0://derecha
					if(x+1 < robotsJava.length && robotsJava[x+1][y]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x+1][y]= robotAMoure;
						robotsJava[x][y]= robotReplicant.construeix(); 
					}
					else if(x+1 < robotsJava.length && robotsJava[x+1][y]!=null) {
						robotAMoure.interactua(robotsJava[x+1][y]);
					}
					break;
				case 1://abajo
					if(y+1 < robotsJava.length && robotsJava[x][y+1]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x][y+1]= robotAMoure;
						robotsJava[x][y]= robotReplicant.construeix(); 
					}
					else if(y+1 < robotsJava.length && robotsJava[x][y+1]!=null) {
						robotAMoure.interactua(robotsJava[x][y+1]);
					}
					break;
				case 2://izquierda
					if(x-1 >= 0 && robotsJava[x-1][y]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x-1][y]= robotAMoure;
						robotsJava[x][y]= robotReplicant.construeix(); 
					}
					else if(x-1 >= 0 && robotsJava[x-1][y]!=null) {
						robotAMoure.interactua(robotsJava[x-1][y]);
					}
					break;
				case 3://arriba
					if(y-1 >= 0 && robotsJava[x][y-1]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x][y-1]= robotAMoure;
						robotsJava[x][y]= robotReplicant.construeix(); 
					}
					else if(y-1 >= 0 && robotsJava[x][y-1]!=null){
						robotAMoure.interactua(robotsJava[x][y-1]);
					}
					break;
				}
			}
			
			else {
				switch (moviment) {
				case 0://derecha
					if(x+1 < robotsJava.length && robotsJava[x+1][y]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x+1][y]= robotAMoure;
						robotsJava[x][y]= null; 
					}
					else if(x+1 < robotsJava.length && robotsJava[x+1][y]!=null){
						robotAMoure.interactua(robotsJava[x+1][y]);
					}
					break;
				case 1://abajo
					if(y+1 < robotsJava.length && robotsJava[x][y+1]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x][y+1]= robotAMoure;
						robotsJava[x][y]= null; 
					}
					else if(y+1 < robotsJava.length && robotsJava[x][y+1]!=null) {
						robotAMoure.interactua(robotsJava[x][y+1]);
					}
					break;
				case 2://izquierda
					if(x-1 >= 0 && robotsJava[x-1][y]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x-1][y]= robotAMoure;
						robotsJava[x][y]= null; 
					}
					else if(x-1 >= 0 && robotsJava[x-1][y]!=null) {
						robotAMoure.interactua(robotsJava[x-1][y]);
					}
					break;
				case 3://arriba
					if(y-1 >= 0 && robotsJava[x][y-1]==null) {
						robotsJava[x][y].gastaEnergia(1);
						robotsJava[x][y-1]= robotAMoure;
						robotsJava[x][y]= null; 
					}
					else if(y-1 >= 0 && robotsJava[x][y-1]!=null) {
						robotAMoure.interactua(robotsJava[x][y-1]);
					}
					break;
				}
			}			
		}
	
	/*---------------------------------------------------------------------------*/
	
	public void mostraTaller() {
		for(int i =0 ; i < robotsJava.length; i++) {
			for(int j =0; j < robotsJava[0].length; j++) {
				System.out.print("|");
				if(robotsJava[i][j] instanceof Replicant == true) {
					System.out.print("R");
				}
				else if(robotsJava[i][j] instanceof Destructor == true) {
					System.out.print("D");
				}
				else if(robotsJava[i][j] instanceof Mecanic == true) {
					System.out.print("M");
				}
				else System.out.print("-");
				System.out.print("|");
			}
			System.out.println("");
		}
		System.out.println("");
		System.out.println("");
		System.out.println("");

	}
	
	/*---------------------------------------------------------------------------*/

	  public void creaTaller() {
		Mecanic robotMecanic = new Mecanic();
		Replicant robotReplicant = new Replicant();
		Destructor robotDestructor = new Destructor();
		
		int posiciox = (int) (Math.random()*5);
		int posicioy = (int) (Math.random()*5);

		
		robotsJava[0][0] = robotMecanic;
		robotsJava[2][2] = robotDestructor;
		robotsJava[4][4] = robotReplicant;
		
		mostraTaller();
		
	}
	
	/*---------------------------------------------------------------------------*/
	public void torn() {
		
	for(int i =0; i < robotsJava.length; i++) {
		for(int j =0; j < robotsJava[0].length; j++) {
			if(robotsJava[i][j]!=null && robotsJava[i][j].decideixSiMou()==true) {
				resolMoviment(robotsJava[i][j], i, j);
			}
		}
	 }
	
	for(int i =0; i < robotsJava.length; i++) {
		for(int j =0; j < robotsJava[0].length; j++) {
			if(robotsJava[i][j]!=null && robotsJava[i][j].decideixSiMou()==true) {
				robotsJava[i][j].haMogut = false;
			}
		}
	 }
	
	mostraTaller();
	
	}
	
}
