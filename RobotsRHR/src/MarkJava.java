import java.util.Random;

public abstract class MarkJava implements java.io.Serializable{
	protected Random sorteig = new Random();
	private int energia = 10;
	public boolean haMogut;
	
	/*------------Clases abstractes--------------*/
	public abstract void interactua(MarkJava altreRobot);
	/*-------------------------------------------*/
	
	public boolean decideixSiMou() {
		if(haMogut==true) {
			haMogut = false;
			return false;
		}
		else {
			return true;
		}
	}
	
	void gastaBatería() {
		energia =0;
	}
	
	void gastaEnergia(int energiaGastada) {
		energia = energia - energiaGastada;
		if(energia<0) energiaGastada =0;
	}
	
	
	public int obteEnergia() {
		return energia;
	}
	
	void recargaBateria() {
		energia = 10;
	}
	
	


}
