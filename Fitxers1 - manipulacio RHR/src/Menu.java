import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Richard Heredia Roca*/
public class Menu {

	public static void main(String[] args) {
		
		int opcio =0;
		boolean sortir = false;
		
		do {
			opcio = presentaMenu();
			sortir=accio(opcio);
		}while(!sortir);

		
	}
	
	
	public static int presentaMenu() {
		Scanner reader = new Scanner(System.in);
		System.out.println("\t\t Manipulaci� de fitxers IES Sabadell");
		System.out.println("Opcio 1: crear txt");
		System.out.println("Opcio 2: escriure txt");
		System.out.println("Opcio 3: llegir txt");
		System.out.println("Opcio 0: sortir");


		System.out.println("Digues la opci� que desitges:");
		int opcio =0;
		opcio = reader.nextInt();
		return opcio;
	}
	
	public static boolean accio(int op) {
		Scanner reader = new Scanner(System.in);
		boolean sortir =false;
		
		switch (op) {
		case 1:
			String nom = "";
			System.out.println("Digues el nom del fitxer que vols crear: ");
			nom = reader.nextLine();
			File obj= new File(nom);
			
			try {
				if(obj.createNewFile()) {
					System.out.println("Fitxer creat: "+ obj.getName());
				}
				else {
					System.out.println("Aquest fitxer Ja existeix.");
				}
			}catch(IOException e) {
				System.out.println("Parse error...");
				e.printStackTrace();
			}
			break;
			
		case 2:
			System.out.println("Digues el nom del fitxer que vols modificar: ");
			nom = reader.nextLine();
			File f = new File(nom);
			FileWriter fw;
			BufferedWriter out;
			String paraula;	
			if(f.exists()) {
			try {
				fw = new FileWriter(f, true);  
					/*SI (afegir == true)
					  		si el fitxer ja existeix afegeix al final. 
					  SINO maxaca el que hi hagi al fitxer
					 */
				out = new BufferedWriter(fw);
				do {
						paraula = reader.nextLine();
						out.write(paraula + "\n");
				} while (paraula.length()>0);
				out.flush();
				out.close(); 
				fw.close();
				//reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			}
			else System.out.println("El archivo no existe cop�n!");
			break;
			
			
		case 3:
			File f2;
			FileReader fr;
			BufferedReader in;
			System.out.println("Digues el nom del fitxer que vols llegir: ");
			nom = reader.nextLine();
			
			f2= new File(nom);	//esto me guarda el archivo	
			try {
				fr = new FileReader(f2);//esto me abre el archivo guardado en r
				in = new BufferedReader(fr);//lee el archivo
				
				while(in.ready()) {
					System.out.println(in.readLine());
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			break;
		default:
			sortir = true;
			System.out.println("Fins aviat!");
			break;
		}
		return sortir;
	}

}
