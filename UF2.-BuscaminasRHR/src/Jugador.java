import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Richard Heredia Roca
 * @version 22/02 alfa (only game)
 * 
 * Clase dedicada al jugador: me presenta el nombre de jugadores y los rankings 
 * */

public class Jugador {
	
	static ArrayList<Integer> punts = new ArrayList<>();
	static ArrayList<String> jugadors = new ArrayList<>();
	static ArrayList<Integer> guanyades=new ArrayList<>();
	

	/**Metodo que me pide el nombre y guarda ese nombre en el arraylist
	 * @return nom : el nom que s'ha guardat en el arraylist*/
	public static String demanaNom(){
		boolean apareix = false;
		
		String nom = "";
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Escriu aqui el nom del jugador: ");
		nom = reader.nextLine().toLowerCase();
		
		apareix = jugadors.contains(nom);
		if(apareix==true) {
			System.out.println("Apareixes en la llista!");
			jugadors.indexOf(nom);
		}
		else {
			jugadors.add(nom);
			jugadors.indexOf(nom);
			punts.add(0);
			guanyades.add(0);
			System.out.println("Benvingut, Jugador "+ nom + ", preparat per jugar?");
		}


		return nom;
	}
	
	/**Metodo que crea la tabla de puntuaciones en un arraylist
	 * @param passat : indica si ja hi ha un nom en la llista
	 */

	public static void ranking(boolean passat) {
		for(int i=0; i < jugadors.size(); i++) {
			System.out.println(jugadors.get(i)+ ": " + punts.get(i) + " punts, " + guanyades.get(i) + " guanyades!");
		}
	}
	
	
	/*-----------------fi---------------------------*/
}
