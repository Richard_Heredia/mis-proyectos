import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Richard Heredia Roca
 * @version 22/02 alfa (only game)
 * 
 * Clase dedicada al juego en s�: el avance del juego, si gana, si pierde etc...
 * falta por implementar: acabar el juego cuando se completa la tabla sin pisar una sola mina
 * */


public class Jugar {

	public static int[][] taulell = new int[8][8]; // inicialitzada a la classe Opcions. En principiant per defecte
	public static int[][] oculta = new int [8][8];

	/**Metodo dedicado al juego: marca los pasos que ha de hacer en cada turno el jugador
	 * @return <ul>
	 * 			<li>true: el jugador pisa mina o gana</li>
	 * 			<li>false: el jugador todav�a sigue en partida</li>
	 * 			</ul> */
	public static boolean joc(String nom) {
		int fila =0;
		int columna =0;
		int punts=0;
		boolean filat=true;//filat, columnat indica a la funcion demanafilacolumna si es una fila o una columna(para el dialogo)
		boolean columnat = false;
		boolean acabat = false;
		boolean correcte = false;
		
		Ajuda.mostraTaulaOculta();
		do {
		fila = demanaFilaColumna(taulell.length, filat);
		columna= demanaFilaColumna(taulell[0].length, columnat);
		correcte = Ajuda.ocupat(oculta, fila, columna);
		}while(correcte==false);
		
		correcte = false;//per a que es torni a poguer utilitzar el bucle
		acabat=finalitzat(oculta, fila, columna,nom);

		if(acabat==false) {
		oculta = analitzarTaula(oculta, fila,columna);
		acabat=finalitzat(oculta, fila, columna,nom);
		punts++;
		Jugador.punts.set(Jugador.jugadors.indexOf(nom), punts);
		}
		return acabat;
	}
	
	/**M�todo implementado para pedir la fila o la columna dependiendo del boolean
	 * en el futuro se trasladar� a Ajuda
	 * @param maxfilacolumna el numero m�ximo de fila/columna
	 * @param filacolumna1: me dice si es columna o fila
	 * @return filacolumna : el numero de fila: columna correcto*/
	

	public static int demanaFilaColumna(int maxfilacolumna, boolean filacolumna1) {
		int filacolumna = 0;
		Scanner reader = new Scanner(System.in);

		do {
			try {
				if(filacolumna1==true) {
				System.out.println("Digues la fila en la que desitjes buscar: ");
				filacolumna = reader.nextInt();
				filacolumna = filacolumna- 1;
				if (filacolumna > maxfilacolumna|| filacolumna < 0)
					System.out.println("numero de fila incorrecta!");
				}
				else {
					System.out.println("Digues la columna en la que desitjes buscar: ");
					filacolumna = reader.nextInt();
					filacolumna = filacolumna- 1;
					if (filacolumna > maxfilacolumna|| filacolumna < 0)
						System.out.println("numero de columna incorrecta!");
				}
			} catch (Exception e) {
				System.out.println("Opci� incorrecta");
				reader.nextLine();
			}
		} while (filacolumna > maxfilacolumna|| filacolumna < 0);
		return filacolumna;
	}
	/**Este metodo revisa el numero de minas que hay alrededor de la casilla seleccionada. 
	 * @param oculta :tabla del jugador
	 * @param fila seleccionada
	 * @param columna seleccionada
	 * @return oculta la tabla del jugador actualizado*/

	public static int[][] analitzarTaula(int[][] oculta, int fila, int columna) {
	
		int numbombas=0;
		
	//creamos la zona a analizar
		int afila= fila -1;
		if(afila < 0) afila=0;
		int acolumna = columna -1;
		if(acolumna < 0) acolumna =0;
	
		
		int maxfila= fila + 2;
		if(maxfila >=oculta.length) maxfila = oculta.length;
		int maxcolumna = columna + 2;
		if(maxcolumna >= oculta[0].length) maxcolumna = oculta[0].length;
		
	//ahora analizamos el numero de bombas alrededor de la zona
		
		for(int i = afila; i < maxfila; i++) {
			for(int j = acolumna; j < maxcolumna; j++) {
				if(oculta[i][j]==-1) numbombas++;
				if(oculta[i][j]!= -1) {
					analitzarTaula(oculta, i , j);
				}
			}
		}
		
		oculta[fila][columna]= numbombas;		
		return oculta;
	}
	/*----------------------------------------------------------------------------------------*/
	
	
	public static void recursiu(int[][] oculta, int fila, int columna) {
		int numbombas=0;
		
	//creamos la zona a analizar
		int afila= fila -1;
		if(afila < 0) afila=0;
		int acolumna = columna -1;
		if(acolumna < 0) acolumna =0;
	
		
		int maxfila= fila + 2;
		if(maxfila >=oculta.length) maxfila = oculta.length;
		int maxcolumna = columna + 2;
		if(maxcolumna >= oculta[0].length) maxcolumna = oculta[0].length;
		
	//ahora analizamos el numero de bombas alrededor de la zona
		
		for(int i = afila; i < maxfila; i++) {
			for(int j = acolumna; j < maxcolumna; j++) {
				if(oculta[i][j]==-1) numbombas++;
			}
		}
		
		oculta[fila][columna]= numbombas;
		System.out.println(oculta[fila][columna]);
	}
	
	
	
	
	/*----------------------------------------------------------------------------------------*/
	
	/**Este metodo me indica si el jugador ha tocado una mina. Si toca, se acaba automaticamente el juego. 
	 *Tambien indica si ha comletado el juego sin haber tocado una mina o no
	 * @param oculta tabla del jugador
	 * @param fila fila seleccionada
	 * @param columna columna seleccionada
	 * @return <ul>
	 * 				<li>true: ha tocado una mina o ha finalizado sin tocar ni una </li>
	 * 				<li>false: no ha tocado mina o no ha acabado de completar la tabla </li>
	 * 				</ul>
	 * */
public static boolean finalitzat (int[][] oculta, int fila, int columna, String nom) {//futura funcion: acabat
	boolean acabat = false;
	int buits=0;
	int guanyat=0;

	if(oculta[fila][columna]==-1) {
		System.out.println("Hi hav�a una mina! S'ha acabat el joc :(");
		acabat = true;
		taulell=oculta;
		Ajuda.mostrataula();
	}
	
	for(int i = 0; i < oculta.length; i++) {
		for(int j = 0; j < oculta[0].length; j++) {
			if(oculta[i][j]==-2) buits++;
		}
	}
	
	if(buits ==0) {
		System.out.println("Has guanyat!");
		System.out.println(Jugador.jugadors.size());
		guanyat=Jugador.guanyades.get(Jugador.jugadors.indexOf(nom));
		guanyat++;
		Jugador.guanyades.set(Jugador.jugadors.indexOf(nom), guanyat);
		acabat = true;
	}
	
	return acabat;
}




	
	
	/*-----------------fi---------------------------*/
}
