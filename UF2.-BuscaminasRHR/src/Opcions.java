import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * @author Richard Heredia Roca
 * @version 22/02 alfa (only game)
 * 
 * Clase dedicada a la dificultad: nos marca el tipo de tablero i el numero de minas. 
 * falta: sistema de ranking y puntuaciones!
 * */

public class Opcions {

	
	/**
	 * Indica las diferentes opciones de dificultat. 
	 * @see creaTaula*/
	public static void dificultat() {
		int opcio = 0;

		Ajuda.menuopcions();
		opcio = Ajuda.opcioCorrecta(4);
		switch (opcio) {
		case 1:
			creaTaula(opcio);
			System.out.println("Dificultat possada en: Principiant!");
			break;
		case 2:
			creaTaula(opcio);
			System.out.println("Dificultat possada en: intermig!");
			break;

		case 3:
			creaTaula(opcio);
			System.out.println("Dificultat possada en: Expert!");

			break;
		case 4:
			creaTaula(opcio);
			System.out.println("Dificultat personalitzada correctament!");
			break;
		}
	}

	/**
	 * dependiendo de la opcion creada en el metodo anterior, nos crea la tabla
	 * @param opcio: la opcion escogida en la dificultad
	 */
	
	public static void creaTaula(int opcio) {
		int mines = 0;
		int posades = 0;
		int fila=0;
		int columna =0;
		
		switch(opcio) {
		case 1:
			// mines =10
			// files=8
			// columnes= 8
			creacio(8,8,10);

			break;
		case 2:
			// mines =40
			// files=16
			// columnes= 16
			
			creacio(16,16,40);
			break;
		case 3:
			//mines =99
			//files=16
			//columnes= 30
			 
			creacio(99,16,30);
			break;
		case 4:	
			int maxfiles=0;
			int maxcolumnes=0;
			int totalcaselles=0;
			
			System.out.println("Digues el numero de files dessitjat: ");
			maxfiles = Ajuda.FilaColumnaCorrecta();
			System.out.println("Digues el numero de columnes dessitjat: ");
			maxcolumnes = Ajuda.FilaColumnaCorrecta();
			totalcaselles = maxfiles*maxcolumnes;
			System.out.println("Digues el numero de mines que vols en el taulell");
			mines = Ajuda.MinesCorrecte(totalcaselles);
		
			creacio(maxfiles,maxcolumnes, mines);
			break;
		}
		
	}
	
	/**
	 * Crea f�sicamente la tabla dependiendo de la dificultad anteriormente dicha.
	 * @param fila maxima a la que llega la tabla
	 * @param columna m�xima a la que llega la tabla
	 * @param mines m�ximas que tiene 
	 */
	public static void creacio(int fila,int columna,int mines) {
		int posades =0;
		int afila=0;
		int acolumna=0;
		Jugar.oculta = new int[fila][columna];
		
		do {
		afila = (int) (Math.random()*fila);
		acolumna= (int) (Math.random()*columna);
		
		if(Jugar.oculta[afila][acolumna]!=1) {
			Jugar.oculta[afila][acolumna]=1;
			posades++;
	   }
		
		}while(posades < mines);
		
		Ajuda.esborra();
	}
	
	/*-----------------fi---------------------------*/
	
}
