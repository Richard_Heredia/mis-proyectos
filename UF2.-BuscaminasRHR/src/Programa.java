import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * <h2>Buscaminas!</h2>
 * @author Richard Heredia Roca
 * @version 22/02 alfa (only game)
 * 
 * Clase dedicada al main: tiene el men� de opciones de las diferentes clases. 
 * falta: sistema de ranking y puntuaciones!
 * */


public class Programa {
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		String jugador=" ";
		int opcio = 0;
		/**Atributo fi: marca el final del programa*/
		boolean fi = false;
		/**Atributo acabat: marca el final del juego*/
		boolean acabat = false;
		/**Atributo passat: me indica si hay un jugador registrado en el ranking*/
		boolean passat= false;
		/**Atributo dificultat: marca si se ha puesto una dificultad antes de empezar a jugar*/
		boolean dificultat = false;
	
		do {
			Ajuda.menu();
			opcio = Ajuda.opcioCorrecta(4);
			
			switch (opcio) {
			
			case 1:
				/**@see Ajuda*/
				Ajuda.ajuda();
				break;
			case 2:
				/**@see Opcions*/
				Opcions.dificultat();
				dificultat = true;
				break;
			case 3:
				if(dificultat == true) {
				/**@see Jugador*/
				jugador=Jugador.demanaNom();
				passat = true;
				do {
				/**@see Jugar*/
				acabat=Jugar.joc(jugador);
				}while(!acabat);
				dificultat = false;
				}
				else System.out.println("Has de possar una dificultat des de Opcions Dificultat!");
				break;
			case 4:
				if(passat ==true) {
					/**@see Jugador*/
					Jugador.ranking(passat);
				}
				else System.out.println("Has de Jugar a una partida per accedir a aquesta opci�");
				break;
			case 0: 
				System.out.println("Fins aviat!");
				fi = true;
				break;
			}
		} while (!fi);

	}
	/*-----------------fi---------------------------*/

}
