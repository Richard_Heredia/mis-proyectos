import java.util.Scanner;

/**
 * @author Richard Heredia Roca
 * @version 22/02 alfa (only game)
 * 
 * Clase dedicada a diferentes ayudas en el programa: ya sea para mostrar un men� c�mo para
 * pedir correctamente una opcion, filas, columnas... 
 * */


public class Ajuda {

	
	/**Muestra el menú general*/
	public static void menu() {
		System.out.println("\t\t -Buscaminas Institut Sabadell-");
		System.out.println("\t1.-Ajuda");
		System.out.println("\t2.-Opcions Dificultat");
		System.out.println("\t3.-Jugar Partida");
		System.out.println("\t4.-Ranking");
		System.out.println("\t0.-Sortir");
		System.out.println("\t----------------------------------------------");
		System.out.println("");
	}
	
	/**Muestra el menú de opciones*/
	public static void menuopcions() {
		System.out.println("Quin nivell vols escollir?");
		System.out.println("1 Nivel principiant (8 x 8)");
		System.out.println("2 Nivel intermig (16 x 16)");
		System.out.println("3 Nivel expert (16 x 30)");
		System.out.println("4 Nivel personalitzat");
	}
	
	/**Muestra las instrucciones del juego.*/
	public static void ajuda() {
		System.out.println("\t\t -Ayuda Buscaminas-");
		System.out
				.println("\tEl juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.");
		System.out.println("\tAlgunas casillas tienen un n�mero, el cual indica la " + "cantidad de minas");
		System.out.println("\tque hay en las casillas circundantes. As�, si una casilla tiene el n�mero 3,");
		System.out.println("\tsignifica que de las ocho casillas que hay alrededor (si no es en una esquina o borde)");
		System.out.println("\thay 3 con minas y 5 sin minas. Si se descubre una casilla sin n�mero indica\")");
		System.out.println("\tque ninguna de las casillas vecinas tiene mina y �stas se descubren autom�ticamente.");
		System.out.println("\tSi se descubre una casilla con una mina se pierde la partida.");
		System.out.println("\t\t Bona sort!");
		System.out.println("");
	}
	
	/**Este m�todo me dice si la opci�n es correcta o no. Est� en diferentes puntos del programa
	 * @param numopcions el n�mero de opciones m�ximo que tiene
	 * @return opcion la opcion escogida correctamente*/

	public static int opcioCorrecta(int numopcions) {
		int opcion = 0;
		Scanner reader = new Scanner(System.in);
		do {
			System.out.println("\t-Seleccioni una opcio:- ");
			try {
				opcion = reader.nextInt();
				if (opcion > numopcions || opcion < 0) {
					System.out.println("Ha de ser una de les 4 opcions!");
				}
			} catch (Exception e) {
				System.out.println("Ha de ser una de les 4 opcions!");
				reader.nextLine();
			}
		} while (opcion > numopcions || opcion < 0);

		return opcion;
	}

	/**Este m�todo se utiliza en la dificultad para marcar el num de filas o columnas correctamente
	 * @return numero: el numero de fila / columna correcto
	 * @see Opcions
	 */
	public static int FilaColumnaCorrecta() {
		int numero = 0;
		Scanner reader = new Scanner(System.in);
		do {
			try {
				numero = reader.nextInt();
				if (numero <= 0)
					System.out.println("El numero ha de ser superior a 0! Torni a posar el numero: ");
			} catch (Exception e) {
				System.out.println("El numero ha de ser un numero positiu!");
				reader.nextLine();
			}
		} while (numero <= 0);

		return numero;
	}
	
	
	/**Este m�todo se utiliza en la dificultad para indicar el n�mero de minas.
	 * @param maxcaselles : el numero de caselles maxim de la taula
	 * @return num el numero deminas correcto
	 * @see Opcions*/
	public static int MinesCorrecte(int maxcaselles) {

		int numero = 0;
		Scanner reader = new Scanner(System.in);
		do {
			try {
				numero = reader.nextInt();
				if (numero <= 0)
					System.out.println("El numero ha de ser superior a 0! Torni a posar el numero: ");
				if (numero >= maxcaselles)
					System.out.println(
							"El numero de mines no pot superar/ser el numero total de caselles: " + maxcaselles);
			} catch (Exception e) {
				System.out.println("El valor ha de ser un numero positiu!");
				reader.nextLine();
			}
		} while (numero <= 0 || numero >= maxcaselles);

		return numero;

	}
	
	/**Muestra la tabla (resultado).*/
	public static void mostrataula() {
		for (int i = 0; i < Jugar.oculta.length; i++) {
			for (int j = 0; j < Jugar.oculta[i].length; j++) {
				System.out.print("| ");
				if (Jugar.taulell[i][j] == -2) {
					System.out.print("-");
				}
				if (Jugar.taulell[i][j] == -1) {
					System.out.print("*");
				}
				if (Jugar.taulell[i][j] != -1 && Jugar.oculta[i][j] != -2) {
					System.out.print("D");
				}
				System.out.print(" |");

			}
			System.out.println(" ");
		}
	}
	
	/**Muestra la tabla (del jugador).*/
	public static void mostraTaulaOculta() {
		
		for (int i = 0; i < Jugar.oculta.length; i++) {
			System.out.print((i+1) + " ");
			for (int j = 0; j < Jugar.oculta[i].length; j++) {
				System.out.print("| ");
				if(Jugar.oculta[i][j]==-2||Jugar.oculta[i][j]==-1)System.out.print("-");
				else System.out.print(Jugar.oculta[i][j]);
				System.out.print(" |");

			}
			System.out.println(" ");
		}
	}
	
	/**Borra y Muestra la tabla (del jugador) con las minas puestas. deja -2 (espacio vac�o) .*/

	public static void esborra() {
		for (int i = 0; i < Jugar.oculta.length; i++) {
			for (int j = 0; j < Jugar.oculta[i].length; j++) {
				System.out.print("| ");
				if(Jugar.oculta[i][j]!=1)Jugar.oculta[i][j]=-2;
				if(Jugar.oculta[i][j]==1) {
					Jugar.oculta[i][j]=-1;
					System.out.print("*");
				}
				else System.out.print(" ");
				System.out.print(" |");
			}
			
			System.out.println(" ");
		}
	}

	/**Aquest metode en diu si la casella que he seleccionat esta ocupada o no
	 * @param taula : la taula del jugador
	 * @param fila : fila seleccinada
	 * @param columna: columna seleccionada
	 * @return <ul>
	 * 			<li>true : si esta ocupada</li>
	 * 			<li>false : si no esta ocupada</li>
	 * 			</ul>*/
		public static boolean ocupat(int[][] taula, int fila, int columna) {
			boolean seleccionat=false;
			System.out.println(taula[fila][columna]);
			if(taula[fila][columna]>=0) {
				System.out.println("Aquesta casella ja ha sigut seleccionada!");
				seleccionat = false;
			}
			else seleccionat = true;
			return seleccionat;
		}
	
	/*-----------------fi---------------------------*/
}
