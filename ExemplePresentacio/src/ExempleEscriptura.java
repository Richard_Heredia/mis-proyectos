import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ExempleEscriptura {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		File f = new File("qFitxer.txt");
		FileWriter fw;
		BufferedWriter out;
		boolean afegir = true;
		String paraula;	
		try {
			fw = new FileWriter(f, afegir);  
				/*SI (afegir == true)
				  		si el fitxer ja existeix afegeix al final. 
				  SINO maxaca el que hi hagi al fitxer
				 */
			out = new BufferedWriter(fw);
			do {
					paraula = reader.nextLine();
					out.write(paraula + "\n");
			} while (paraula.length()>0);
			out.flush();
			out.close(); fw.close();
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
