import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
public class ExempleLectura {

	public static void main(String[] args) {

		File f;
		FileReader fr;
		BufferedReader in;
		
		f= new File("dades.txt");	//esto me guarda el archivo	
		try {
			fr = new FileReader(f);//esto me abre el archivo guardado en r
			in = new BufferedReader(fr);//lee el archivo
			
			while(in.ready()) {
				System.out.println(in.readLine());
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
