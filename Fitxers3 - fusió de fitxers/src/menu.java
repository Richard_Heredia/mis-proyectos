import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author Richard Heredia
 * */
public class menu {

	public static void main(String[] args) {
		
		int opcio =0;
		boolean sortir = false;
		
		do {
			opcio = presentaMenu();
			sortir=accio(opcio);
		}while(!sortir);
		
		
		
	}
	
	/**
	 * Aquesta funci presenta el men y retorna la opci
	 * @return opcio = La opci seleccionada
	 * */
	public static int presentaMenu() {
		Scanner reader = new Scanner(System.in);
		System.out.println("\t\t Manipulaci de fitxers IES Sabadell");
		System.out.println("Opcio 1: modificar/crear txt");
		System.out.println("Opcio 2: llegir txt");
		System.out.println("Opcio 3: fusionar txt");
		System.out.println("Opcio 0: sortir");


		System.out.println("Digues la opci que desitges:");
		int opcio =0;
		opcio = reader.nextInt();
		return opcio;
	}
	
	/**
	 * Aquesta funci presenta la acci que prendr depenent de la opcio seleccionada amb anterioritat.
	 * @param op = la opci seleccionada
	 * @return sortir = True si es selecciona una opcio fora dels cases / false si s'ha seleccionat una opcio dins dels cases 
	 *
	 * */
	public static boolean accio(int op) {
		Scanner reader = new Scanner(System.in);
		boolean sortir =false;
		String nom = "";

		
		switch (op) {
			
		case 1:
			modificaTxt();
			break;
			
			
		case 2:
			llegirTxt();
			break;
			
		case 3:
			fusionarTxt();
			break;
			
		default:
			sortir = true;
			System.out.println("Fins aviat!");
			break;
		}
		return sortir;
	}
	
	/**
	 * Funcio que modifica els txt que es posin. Si no existeix, no es modificar res.
	 * */
	public static void modificaTxt() {
		String nom = "";
		Scanner reader = new Scanner(System.in);
		System.out.println("Digues el nom del fitxer que vols modificar: ");
		nom = reader.nextLine();
		File f = new File(nom);
		FileWriter fw;
		BufferedWriter out;
		String paraula;	

		try {
			fw = new FileWriter(f, true);  
				/*SI (afegir == true)
				  		si el fitxer ja existeix afegeix al final. 
				  SINO maxaca el que hi hagi al fitxer
				 */
			out = new BufferedWriter(fw);
			do {
					paraula = reader.nextLine();
					out.write(paraula + "\n");
			} while (paraula.length()>0);
			out.flush();
			out.close(); 
			fw.close();
			//reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Funci que llegeix els txt que existeixin. Si no existeix, la funci no far res
	 * */
	public static void llegirTxt() {
		File f2;
		FileReader fr;
		BufferedReader in;
		String nom = "";
		Scanner reader = new Scanner(System.in);
		System.out.println("Posa la ruta del arxiu que vulguis obrir/crear: ");
		nom = reader.nextLine();
		
		f2= new File(nom);	//esto me guarda el archivo	
		if(f2.exists()) {
			try {
				fr = new FileReader(f2);//esto me abre el archivo guardado en r
				in = new BufferedReader(fr);//lee el archivo
				
				while(in.ready()) {
					System.out.println(in.readLine());
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		else System.out.println("Aquest arxiu no existeix!");

	}

	/**
	 * Funci que fusiona els txt que existeixin. Si no existeix cap dels dos, la funci no far res
	 * */
	public static void fusionarTxt() {
		String nom1 ="";
		String nom2 ="";
		String nom3 = "";
		int contadorEspais =0;
		Scanner reader = new Scanner(System.in);
		System.out.println("AVIS: Nomes es fusionaran els arxius que tinguin no mes de dos espais de separaci"
				+ "entre linia i linia");
		/*-----------------------------------------------------------------*/
		System.out.println("Digues el nom del fitxer 1 que vols llegir: ");
		nom1 = reader.nextLine();
		File f1 = new File(nom1);
		FileReader fr1;
		BufferedReader in;
		/*-----------------------------------------------------------------*/
		System.out.println("Digues el nom del fitxer amb el que el vols fusionar: ");
		nom2 = reader.nextLine();
		File f2 = new File(nom2);
		FileReader fr2;
		BufferedReader in2;
		/*-----------------------------------------------------------------*/

		String paraula;	
		if(f1.exists() && f2.exists()) {
			System.out.println("Digues el nom del fitxer de la fusió: ");
			nom3= reader.nextLine();
			File f3 = new File(nom3);
			FileWriter fw;
			BufferedWriter out;

		try {
			/*-------------------Hem permet escriure------------------------*/
			fw = new FileWriter(f3, true);  
				/*SI (afegir == true)
				  		si el fitxer ja existeix afegeix al final. 
				  SINO maxaca el que hi hagi al fitxer
				 */
			out = new BufferedWriter(fw);
			/*-------------------Hem permet llegir------------------------*/

			fr1 = new FileReader(f1);
			in = new BufferedReader(fr1);
			
			
			do {
					paraula = in.readLine();
					if(paraula == null) {
						contadorEspais++;
						paraula =" ";
					}
					//System.out.println(paraula);
					out.write(paraula + "\n");
			} while (contadorEspais <3 && in.ready());
			contadorEspais =0;

			/*-----------------------------------------------------------------*/

			fr2 = new FileReader(f2);
			in2 = new BufferedReader(fr2);
			
			
			do {
					paraula = in2.readLine();
					if(paraula == null) {
						contadorEspais++;
						paraula =" ";
					}
					System.out.println(paraula);
					out.write(paraula + "\n");
			} while (contadorEspais <3 && in2.ready());
			out.flush();
			out.close(); 
			fw.close();

			/*-----------------------------------------------------------------*/

			System.out.println("Operació exitosa!");
			System.out.println("");
			System.out.println("");
			/*Per tornar a llegir el arxiu modificat: */
			System.out.println("Resultat de la operaci: ");
			FileReader ffinal;
			ffinal = new FileReader(f3);
			in = new BufferedReader(ffinal);
			
			while(in.ready()) {
				System.out.println(in.readLine());
			}
			/*-----------------------------------------*/
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		}
		else System.out.println("El archivo no existe!");
	
	}
	
	
	/*Fi de la clase*/
}
