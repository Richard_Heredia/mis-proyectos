import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
/**
 * @author Richard Heredia Roca*/
public class main {

	public static void main(String[] args) {
		// TODO Esbozo de m�todo generado autom�ticamente
		File f2;
		FileReader fr;
		BufferedReader in;
		Scanner reader = new Scanner(System.in);
		String nom ="";
		boolean continuar = false;
		int contador =0;
		char opcio = ' ';
		System.out.println("Posa la ruta completa del arxiu .txt que vulguis modificar: ");
		nom = reader.nextLine();
		
		f2= new File(nom);	//esto me guarda el archivo	
		if(f2.exists()) {
		try {
			fr = new FileReader(f2);//esto me abre el archivo guardado en r
			in = new BufferedReader(fr);//lee el archivo
			
			
			
			while (in.ready() && !continuar) {
				while (contador < 4 && in.ready()){
					System.out.println(in.readLine());
					contador++;
					if (contador == 3) {
						System.out.println("Vols continuar? s--S�/altre lletra o numero -- No");
						opcio = reader.next().toLowerCase().charAt(0);
						if (opcio == 's') {
							contador = 0;
						} else {
							continuar = true;
							contador = 4;
						}
					}
				} 
			}
		
			System.out.println("F� de la lectura de " + f2.getName());
			
		}catch(IOException e) {
			e.printStackTrace();
		}
		}else {
			System.out.println("No existe el archivo!");
		}
	}

}
