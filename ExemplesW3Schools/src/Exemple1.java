import java.io.File;
import java.io.IOException;
public class Exemple1 {

	public static void main(String[] args) {
		File obj= new File("created.txt");
		try {
			if(obj.createNewFile()) {
				System.out.println("File created: "+ obj.getName());
			}
			else {
				System.out.println("File alredy exists.");
			}
		}catch(IOException e) {
			System.out.println("Algo ha salido mal...");
			e.printStackTrace();
		}
	}

}
