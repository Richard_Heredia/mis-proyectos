import java.util.Scanner;

public class ex2_FIbbonacciDeParells {

	public static void main(String[] args) {
		int fibo = 1;
		int anterior = 0;
		int numero = 0;
		int[] resultats;
		int seguent =0;
		Scanner reader = new Scanner(System.in);
		boolean parells = false;
		
		while (numero < 2) {
			try {
				System.out.println("Digui el numero desitjat: ");
				numero = reader.nextInt();
				if (numero < 2)
					System.out.println("El numero ha de ser positius!");
			} catch (Exception e) {
				System.out.println("Ha de ser un numero enter!");
				reader.nextLine();
			}
		}

		resultats = new int[numero];

		for (int i = 0; i < numero; i++) {
			fibo = fibo + anterior;
			anterior = fibo - anterior;
			if (i == 1) {
				fibo = 1;
				resultats[i] = fibo;
			} else {
				resultats[i] = fibo;
			}
			if (resultats[i] % 2 == 0)
				parells = true;
		}

		System.out.print("Els " + numero + " primers elements de Fibbonacci són: ");
		for (int i = 0; i < resultats.length; i++) {
			System.out.print(resultats[i]);
			if( i != resultats.length-1) System.out.print(", ");
		}

		System.out.println(" ");

		if (parells == true) {
			System.out.print("Els elements parells continguts seríen: ");

			for (int i = 0; i < resultats.length; i++) {
				if (i == 0)
					System.out.print("{");
				if (resultats[i] % 2 == 0) {
					System.out.print(resultats[i]);
					
				}
				if(i!=resultats.length-1)seguent = i+1;
				else i= resultats.length-1;
				if(resultats[seguent]%2==0 && i !=0 && i!=1)System.out.print(", ");
				
				
				if (i == resultats.length - 1)
					System.out.print("}");					
			}
		} else
			System.out.println("No hi ha cap parell en aquesta seqüencia!");
		reader.close();
	}

}
