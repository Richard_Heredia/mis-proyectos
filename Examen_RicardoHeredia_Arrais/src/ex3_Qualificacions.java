import java.util.Scanner;
public class ex3_Qualificacions {

	
	//nota del autor
	//el programa pide primero las primeras notas y luego pide las segundas notas de cada alumno
	//se ha hecho con la intencion de corroborar que las notas esten bien puestas y no
	//se introduzcan letras.
	//Lo escribo para ahorrar tiempo de correccion
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int numero =0;
		float numero2=0;
		String[]noms;
		float nota1[];
		float nota2[];
		float notaf[];
		boolean correcte = false;
		int k=0;
		
		while (numero <= 0) {
			try {
				System.out.println("Digui el numero desitjat: ");
				numero = reader.nextInt();
				if (numero <= 0)
					System.out.println("El numero ha de ser positius!");
			} catch (Exception e) {
				System.out.println("Ha de ser un numero enter!");
				reader.nextLine();
			}
		}
		
		noms = new String[numero];
		nota1= new float[numero];
		nota2 = new float[numero];
		notaf = new float[numero];
		reader.nextLine();
		
		for(int i =0; i < noms.length;i++) {
			System.out.println("digues el nom del alumne: "+ (i+1));
			noms[i]= reader.nextLine();
		}
		
		
		while (k<nota1.length) {
			try {
				System.out.print("Escriu la nota 1 de: " + noms[k]);
				numero2 = reader.nextFloat();
				if (numero2 >= 0 || numero2 <=10) {
				nota1[k]=numero2;
				k++;
				}
				else System.out.println("La nota ha de ser un numero entre 0 y 10!");
				
			} catch (Exception e) {
				System.out.println("Ha de ser un numero");
				reader.nextLine();
			}
		}
		
		k=0;
		numero2=0;
		
		while (k<nota2.length) {
			try {
				System.out.print("Escriu la nota 2 de: " + noms[k]);
				numero2 = reader.nextFloat();
				if (numero2 >= 0 || numero2 <=10) {
				nota2[k]=numero2;
				k++;
				}
				else System.out.println("La nota ha de ser un numero entre 0 y 10!");
				
			} catch (Exception e) {
				System.out.println("Ha de ser un numero");
				reader.nextLine();
			}
		}
		
		System.out.print("Notes del primer examen (per comrovar): ");
		for(int h=0; h<nota1.length;h++) {
			System.out.print(nota1[h] + " ");
		}
		
		System.out.println(" ");
		
		System.out.print("Notes del segon examen (per comrovar): ");
		for(int h=0; h<nota2.length;h++) {
			System.out.print(nota2[h] + " ");
		}
		
		System.out.println(" ");

		
		for(int l=0; l <notaf.length;l++) {
			notaf[l]= (nota1[l]+nota2[l])/2;
		}
		
		for(int l=0; l <notaf.length;l++) {
			System.out.println(noms[l] +" : "+ notaf[l]);
		}

		reader.close();
	}

}
